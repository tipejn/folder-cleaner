﻿Imports System.Windows.Forms

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ucClosingPanel
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblFinalMsg = New System.Windows.Forms.Label()
        Me.lblRegained = New System.Windows.Forms.Label()
        Me.btnShowFiles = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblCongratulations
        '
        Me.lblFinalMsg.AutoSize = True
        Me.lblFinalMsg.Font = New System.Drawing.Font("Century Gothic", 27.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.lblFinalMsg.Location = New System.Drawing.Point(117, 85)
        Me.lblFinalMsg.Name = "lblCongratulations"
        Me.lblFinalMsg.Size = New System.Drawing.Size(316, 44)
        Me.lblFinalMsg.TabIndex = 0
        Me.lblFinalMsg.Text = "Congratulations!"
        '
        'lblRegained
        '
        Me.lblRegained.AutoSize = True
        Me.lblRegained.Font = New System.Drawing.Font("Century Gothic", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.lblRegained.Location = New System.Drawing.Point(121, 149)
        Me.lblRegained.Name = "lblRegained"
        Me.lblRegained.Size = New System.Drawing.Size(249, 24)
        Me.lblRegained.TabIndex = 1
        Me.lblRegained.Text = "You regained @mb MB!"
        '
        'btnShowFiles
        '
        Me.btnShowFiles.BackColor = System.Drawing.Color.FromArgb(CType(CType(178, Byte), Integer), CType(CType(8, Byte), Integer), CType(CType(56, Byte), Integer))
        Me.btnShowFiles.FlatAppearance.BorderSize = 0
        Me.btnShowFiles.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnShowFiles.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnShowFiles.ForeColor = System.Drawing.Color.White
        Me.btnShowFiles.Location = New System.Drawing.Point(125, 216)
        Me.btnShowFiles.Name = "btnShowFiles"
        Me.btnShowFiles.Size = New System.Drawing.Size(150, 41)
        Me.btnShowFiles.TabIndex = 2
        Me.btnShowFiles.Text = "Show files"
        Me.btnShowFiles.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(178, Byte), Integer), CType(CType(8, Byte), Integer), CType(CType(56, Byte), Integer))
        Me.btnClose.FlatAppearance.BorderSize = 0
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnClose.ForeColor = System.Drawing.Color.White
        Me.btnClose.Location = New System.Drawing.Point(281, 216)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(150, 41)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'ucCongratulation
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnShowFiles)
        Me.Controls.Add(Me.lblRegained)
        Me.Controls.Add(Me.lblFinalMsg)
        Me.Name = "ucCongratulation"
        Me.Size = New System.Drawing.Size(1163, 412)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblFinalMsg As Label
    Friend WithEvents lblRegained As Label
    Friend WithEvents btnShowFiles As Button
    Friend WithEvents btnClose As Button
End Class
