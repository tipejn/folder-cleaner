﻿Option Strict On
Imports System.IO

Public Class DaysDiffManager
    Private Const DefaultLastModifiedDays As Integer = 5
    Private pm As PathManager
    Private daysCount As Integer

    Public Sub New()
        pm = New PathManager
        Read()
    End Sub

    Public ReadOnly Property LastModifiedDaysCount() As Integer
        Get
            Return daysCount
        End Get
    End Property

    Private Sub Read()

        Dim lastModifiedFilePath As String = pm.GetLastModifiedPath

        If Not File.Exists(lastModifiedFilePath) Then
            SetNewValue(DefaultLastModifiedDays)
        Else
            Dim myReader As StreamReader = File.OpenText(lastModifiedFilePath)
            With myReader
                daysCount = Convert.ToInt32(.ReadLine())
                .Close()
            End With
        End If

    End Sub

    Private Sub Save()
        Dim myStream As StreamWriter = New StreamWriter(pm.GetLastModifiedPath, False)
        With myStream
            .WriteLine(LastModifiedDaysCount)
            .Close()
        End With
    End Sub

    Public Sub SetNewValue(value As Integer)
        daysCount = value
        Save()
    End Sub

End Class
