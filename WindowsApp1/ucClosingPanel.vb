﻿Imports System
Imports System.Diagnostics

Public Class ucClosingPanel
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        frmMain.Dispose()
    End Sub

    Private Sub btnShowFiles_Click(sender As Object, e As EventArgs) Handles btnShowFiles.Click
        Dim pm As PathManager = New PathManager
        Process.Start(pm.GetBackupPath)
    End Sub
End Class
