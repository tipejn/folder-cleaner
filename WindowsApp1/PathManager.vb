﻿Imports System
Imports System.IO

Public Class PathManager
    Private Const directoriesFileName As String = "directories.txt"
    Private Const backupFolderName As String = "back"
    Private Const projectFolderName As String = "Folder Cleaner"
    Private Const lastModifiedFileName As String = "lastModified.txt"

    Private ReadOnly myDocumentsPath As String
    Private ReadOnly projectPath As String

    Public Sub New()
        myDocumentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        projectPath = Path.Combine(myDocumentsPath, projectFolderName) 'myDocumentsPath & "\" & projectFolderName
    End Sub

    Public ReadOnly Property GetDirectoriesPath() As String
        Get
            Return Path.Combine(projectPath, directoriesFileName) 'projectPath & "\" & directoriesFileName
        End Get
    End Property

    Public ReadOnly Property GetLastModifiedPath() As String
        Get
            Return Path.Combine(projectPath, lastModifiedFileName) 'projectPath & "\" & lastModifiedFileName
        End Get
    End Property

    Public ReadOnly Property GetBackupPath() As String
        Get
            Return Path.Combine(projectPath, backupFolderName) 'projectPath & "\" & backupFolderName
        End Get
    End Property

    Public ReadOnly Property GetDesktopPath() As String
        Get
            Return Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
        End Get
    End Property

End Class
