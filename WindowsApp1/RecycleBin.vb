﻿Imports System

Module RecycleBin
    Private Declare Function SHEmptyRecycleBin Lib "shell32.dll" Alias "SHEmptyRecycleBinA" (ByVal hWnd As Int32, ByVal pszRootPath As String, ByVal dwFlags As Int32) As Int32
    Private Declare Function SHUpdateRecycleBinIcon Lib "shell32.dll" () As Int32

    Private Const SHERB_NOCONFIRMATION As Integer = &H1
    Private Const SHERB_NOPROGRESSUI As Integer = &H2
    Private Const SHERB_NOSOUND As Integer = &H4

    Public Sub EmptyRecycleBin()
        SHEmptyRecycleBin(CInt(IntPtr.Zero), "", SHERB_NOCONFIRMATION + SHERB_NOSOUND)
        SHUpdateRecycleBinIcon()
    End Sub
End Module
