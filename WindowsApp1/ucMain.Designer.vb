﻿Imports System.Windows.Forms

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ucMain
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lblLastModifiedDays = New System.Windows.Forms.Label()
        Me.lblDirectories = New System.Windows.Forms.Label()
        Me.lbDirectories = New System.Windows.Forms.ListBox()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnRemove = New System.Windows.Forms.Button()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.txtEditLastModifiedDays = New System.Windows.Forms.TextBox()
        Me.lblLastModifiedValue = New System.Windows.Forms.Label()
        Me.btnEditOk = New System.Windows.Forms.Button()
        Me.errTxtEdit = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblLastModifiedDays
        '
        Me.lblLastModifiedDays.AutoSize = True
        Me.lblLastModifiedDays.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.lblLastModifiedDays.Location = New System.Drawing.Point(47, 26)
        Me.lblLastModifiedDays.Name = "lblLastModifiedDays"
        Me.lblLastModifiedDays.Size = New System.Drawing.Size(274, 21)
        Me.lblLastModifiedDays.TabIndex = 0
        Me.lblLastModifiedDays.Text = "Days difference since last change:"
        '
        'lblDirectories
        '
        Me.lblDirectories.AutoSize = True
        Me.lblDirectories.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.lblDirectories.Location = New System.Drawing.Point(47, 66)
        Me.lblDirectories.Name = "lblDirectories"
        Me.lblDirectories.Size = New System.Drawing.Size(95, 21)
        Me.lblDirectories.TabIndex = 1
        Me.lblDirectories.Text = "Directories:"
        '
        'lbDirectories
        '
        Me.lbDirectories.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.lbDirectories.FormattingEnabled = True
        Me.lbDirectories.Location = New System.Drawing.Point(51, 90)
        Me.lbDirectories.Name = "lbDirectories"
        Me.lbDirectories.Size = New System.Drawing.Size(900, 234)
        Me.lbDirectories.TabIndex = 3
        '
        'btnEdit
        '
        Me.btnEdit.BackColor = System.Drawing.Color.FromArgb(CType(CType(178, Byte), Integer), CType(CType(8, Byte), Integer), CType(CType(56, Byte), Integer))
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnEdit.ForeColor = System.Drawing.Color.White
        Me.btnEdit.Location = New System.Drawing.Point(387, 17)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(139, 38)
        Me.btnEdit.TabIndex = 4
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = False
        '
        'btnAdd
        '
        Me.btnAdd.BackColor = System.Drawing.Color.FromArgb(CType(CType(178, Byte), Integer), CType(CType(8, Byte), Integer), CType(CType(56, Byte), Integer))
        Me.btnAdd.FlatAppearance.BorderSize = 0
        Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdd.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnAdd.ForeColor = System.Drawing.Color.White
        Me.btnAdd.Location = New System.Drawing.Point(957, 90)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(139, 38)
        Me.btnAdd.TabIndex = 5
        Me.btnAdd.Text = "Add"
        Me.btnAdd.UseVisualStyleBackColor = False
        '
        'btnRemove
        '
        Me.btnRemove.BackColor = System.Drawing.Color.FromArgb(CType(CType(178, Byte), Integer), CType(CType(8, Byte), Integer), CType(CType(56, Byte), Integer))
        Me.btnRemove.FlatAppearance.BorderSize = 0
        Me.btnRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRemove.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnRemove.ForeColor = System.Drawing.Color.White
        Me.btnRemove.Location = New System.Drawing.Point(957, 134)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(139, 38)
        Me.btnRemove.TabIndex = 6
        Me.btnRemove.Text = "Remove"
        Me.btnRemove.UseVisualStyleBackColor = False
        '
        'btnStart
        '
        Me.btnStart.BackColor = System.Drawing.Color.FromArgb(CType(CType(178, Byte), Integer), CType(CType(8, Byte), Integer), CType(CType(56, Byte), Integer))
        Me.btnStart.FlatAppearance.BorderSize = 0
        Me.btnStart.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStart.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnStart.ForeColor = System.Drawing.Color.White
        Me.btnStart.Location = New System.Drawing.Point(812, 330)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(139, 38)
        Me.btnStart.TabIndex = 7
        Me.btnStart.Text = "Clean"
        Me.btnStart.UseVisualStyleBackColor = False
        '
        'txtEdit
        '
        Me.txtEditLastModifiedDays.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtEditLastModifiedDays.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.txtEditLastModifiedDays.Location = New System.Drawing.Point(318, 26)
        Me.txtEditLastModifiedDays.Name = "txtEdit"
        Me.txtEditLastModifiedDays.Size = New System.Drawing.Size(35, 20)
        Me.txtEditLastModifiedDays.TabIndex = 8
        Me.txtEditLastModifiedDays.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.txtEditLastModifiedDays.Visible = False
        '
        'lblLastModifiedValue
        '
        Me.lblLastModifiedValue.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.lblLastModifiedValue.Location = New System.Drawing.Point(316, 26)
        Me.lblLastModifiedValue.Name = "lblLastModifiedValue"
        Me.lblLastModifiedValue.Size = New System.Drawing.Size(39, 21)
        Me.lblLastModifiedValue.TabIndex = 9
        Me.lblLastModifiedValue.Text = "555"
        Me.lblLastModifiedValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnEditOk
        '
        Me.btnEditOk.BackColor = System.Drawing.Color.FromArgb(CType(CType(178, Byte), Integer), CType(CType(8, Byte), Integer), CType(CType(56, Byte), Integer))
        Me.btnEditOk.FlatAppearance.BorderSize = 0
        Me.btnEditOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditOk.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.btnEditOk.ForeColor = System.Drawing.Color.White
        Me.btnEditOk.Location = New System.Drawing.Point(387, 17)
        Me.btnEditOk.Name = "btnEditOk"
        Me.btnEditOk.Size = New System.Drawing.Size(139, 38)
        Me.btnEditOk.TabIndex = 10
        Me.btnEditOk.Text = "OK"
        Me.btnEditOk.UseVisualStyleBackColor = False
        Me.btnEditOk.Visible = False
        '
        'errTxtEdit
        '
        Me.errTxtEdit.AutoSize = True
        Me.errTxtEdit.Font = New System.Drawing.Font("Century Gothic", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.errTxtEdit.ForeColor = System.Drawing.Color.Red
        Me.errTxtEdit.Location = New System.Drawing.Point(293, 9)
        Me.errTxtEdit.Name = "errTxtEdit"
        Me.errTxtEdit.Size = New System.Drawing.Size(84, 15)
        Me.errTxtEdit.TabIndex = 11
        Me.errTxtEdit.Text = "Numbers only!"
        Me.errTxtEdit.Visible = False
        '
        'ucMain
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.Controls.Add(Me.errTxtEdit)
        Me.Controls.Add(Me.btnEditOk)
        Me.Controls.Add(Me.lblLastModifiedValue)
        Me.Controls.Add(Me.txtEditLastModifiedDays)
        Me.Controls.Add(Me.btnStart)
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.btnEdit)
        Me.Controls.Add(Me.lbDirectories)
        Me.Controls.Add(Me.lblDirectories)
        Me.Controls.Add(Me.lblLastModifiedDays)
        Me.Name = "ucMain"
        Me.Size = New System.Drawing.Size(1163, 412)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblLastModifiedDays As Label
    Friend WithEvents lblDirectories As Label
    Friend WithEvents lbDirectories As ListBox
    Friend WithEvents btnEdit As Button
    Friend WithEvents btnAdd As Button
    Friend WithEvents btnRemove As Button
    Friend WithEvents btnStart As Button
    Friend WithEvents txtEditLastModifiedDays As TextBox
    Friend WithEvents lblLastModifiedValue As Label
    Friend WithEvents btnEditOk As Button
    Friend WithEvents errTxtEdit As Label
End Class
