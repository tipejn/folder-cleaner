﻿Imports System.IO

Public Class ucConfirmation

    Private Sub btnNo_Click(sender As Object, e As EventArgs) Handles btnNo.Click
        frmMain.ucMain.BringToFront()
    End Sub

    Private Sub btnYes_Click(sender As Object, e As EventArgs) Handles btnYes.Click

        Dim cleaner As Cleaner = New Cleaner(frmMain.ucMain.GetDirectories, frmMain.ucMain.GetLastModifiedDays)
        Dim regainedSpace As Double

        With cleaner
            .RemoveBackupedFiles()
            .CleanDirectories()
            regainedSpace = .GetRegainedSpace
        End With

        RecycleBin.EmptyRecycleBin()

        With frmMain.ucCongratulation
            If regainedSpace > 0 Then
                .lblFinalMsg.Text = "Congratulations!"
                .lblRegained.Text = String.Format("You regained {0} MB!", regainedSpace)
            Else

                .lblFinalMsg.Text = "No files deleted."
                .lblRegained.Visible = False
                .btnShowFiles.Visible = False
                .btnClose.Left = .btnShowFiles.Left
            End If
            .BringToFront()
        End With
    End Sub

End Class
