﻿Option Strict On
Imports System.ComponentModel
Imports System.IO

Public Class ucMain
    Private dm As DirectoriesManager
    Private ddm As DaysDiffManager

    Public ReadOnly Property GetLastModifiedDays() As Integer
        Get
            Return Convert.ToInt32(lblLastModifiedValue.Text)
        End Get
    End Property

    Public ReadOnly Property GetDirectories() As List(Of String)
        Get
            Return lbDirectories.Items.Cast(Of String).ToList()
        End Get
    End Property

    Private Sub ucMain_Load(sender As Object, e As EventArgs) Handles Me.Load
        dm = New DirectoriesManager
        ddm = New DaysDiffManager
        SetLastModifiedLabel()
        PopulateDirectories()
    End Sub

    Private Sub SetLastModifiedLabel()
        lblLastModifiedValue.Text = ddm.LastModifiedDaysCount.ToString
    End Sub

    Private Sub PopulateDirectories()
        With lbDirectories.Items
            .Clear()
            For Each directoryPath As String In dm.GetDirectories()
                .Add(directoryPath)
            Next
        End With
    End Sub

    Public Sub CheckDirectories()
        With lbDirectories
            If .Items.Count = 0 Then
                Throw New NoDirectoriesException("Add directory first.")
            End If
            Dim i As Integer = 0
            For Each directoryPath As String In .Items
                If Not Directory.Exists(directoryPath) Then
                    .SetSelected(i, True)
                    Dim errMsg As String = String.Format("Directory '{0}' does not exist. Do you want to remove it from list?", Path.GetDirectoryName(directoryPath))
                    Throw New FolderDoesNotExistException(errMsg)
                End If
                i += 1
            Next
        End With
    End Sub

    Public Function Confirm(msg As String) As Boolean
        Return MessageBox.Show(msg, "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes
    End Function

    Private Sub btnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click

        Try
            CheckDirectories()
            frmMain.ucConfirmation.BringToFront()
        Catch ex As NoDirectoriesException
            MessageBox.Show(ex.Message, "No directories", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        Catch ex As FolderDoesNotExistException
            If Confirm(ex.Message) Then
                dm.removeDirectory(lbDirectories.SelectedItem().ToString)
                PopulateDirectories()
            End If
        Catch ex As Exception
            MessageBox.Show("Unknown error", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click

        With lbDirectories
            If Not .SelectedItem() Is Nothing Then
                dm.removeDirectory(.SelectedItem().ToString)
                PopulateDirectories()
            Else
                MessageBox.Show("Select directory first.", "Nothing selected", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End With

    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Dim fd As FolderBrowserDialog = New FolderBrowserDialog

        With fd
            .RootFolder = Environment.SpecialFolder.Desktop
            .Description = "Select folder to clean"
            If .ShowDialog() = DialogResult.OK Then
                dm.addDirectory(.SelectedPath)
            End If
        End With

        PopulateDirectories()

    End Sub

    Private Sub btnEdit_Click(sender As Object, e As EventArgs) Handles btnEdit.Click

        lblLastModifiedValue.Visible = False
        txtEditLastModifiedDays.Visible = True
        btnEditOk.Visible = True
        txtEditLastModifiedDays.Select()
        txtEditLastModifiedDays.SelectAll()

    End Sub

    Private Sub btnEditOk_Click(sender As Object, e As EventArgs) Handles btnEditOk.Click

        lblLastModifiedValue.Visible = True
        txtEditLastModifiedDays.Visible = False
        btnEditOk.Visible = False
        ddm.SetNewValue(Convert.ToInt32(txtEditLastModifiedDays.Text))
        SetLastModifiedLabel()

    End Sub

    Private Sub txtEditLastModifiedDays_Validating(sender As Object, e As CancelEventArgs) Handles txtEditLastModifiedDays.Validating
        With txtEditLastModifiedDays
            If Not IsNumeric(.Text) Then
                e.Cancel = True
                .SelectAll()
                errTxtEdit.Visible = True
            End If
        End With
    End Sub

    Private Sub txtEditLastModifiedDays_Validated(sender As Object, e As EventArgs) Handles txtEditLastModifiedDays.Validated
        errTxtEdit.Visible = False
    End Sub
End Class
