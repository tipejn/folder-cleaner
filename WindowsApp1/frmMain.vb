﻿Imports System
Imports System.IO
Imports System.Windows.Forms

Public Class frmMain

    Public Sub frmMain_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.ucMain.BringToFront()
    End Sub

    Public Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Me.Dispose()
    End Sub

End Class
