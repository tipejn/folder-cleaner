﻿Imports System

Public Class FolderDoesNotExistException : Inherits System.Exception
    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(message As String)
        MyBase.New(message)
    End Sub

    Public Sub New(message As String, inner As System.Exception)
        MyBase.New(message, inner)
    End Sub
End Class

Public Class NoDirectoriesException : Inherits System.Exception
    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(message As String)
        MyBase.New(message)
    End Sub

    Public Sub New(message As String, inner As System.Exception)
        MyBase.New(message, inner)
    End Sub
End Class
