﻿Imports System
Imports System.Collections.Generic
Imports System.IO
Imports System.Linq

Public Class DirectoriesManager
    Private pm As PathManager
    Private ReadOnly directoryPath As String
    Private directories As List(Of String)


    Public Sub New()
        pm = New PathManager
        directories = New List(Of String)
        directoryPath = pm.GetDirectoriesPath
        FillDirectories()
    End Sub

    Public ReadOnly Property GetDirectories() As List(Of String)
        Get
            Return directories
        End Get
    End Property

    Private Sub FillDirectories()
        If File.Exists(directoryPath) Then
            directories = File.ReadAllLines(directoryPath).ToList
        Else
            addDirectory(Environment.GetFolderPath(Environment.SpecialFolder.Desktop))
        End If
    End Sub

    Public Sub addDirectory(path As String)
        directories.Add(path)
        Dim myStream As StreamWriter = New StreamWriter(directoryPath, True)
        myStream.WriteLine(path)
        myStream.Close()
    End Sub

    Public Sub removeDirectory(path As String)
        directories.Remove(path)
        Dim myStream As StreamWriter = New StreamWriter(directoryPath, False)
        For Each myDirectory As String In directories
            myStream.WriteLine(myDirectory)
        Next
        myStream.Close()
    End Sub

End Class
