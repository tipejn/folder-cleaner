﻿Imports System.IO

Public Class Cleaner
    Private Const ForbiddenExtensionsString As String = ".ink|.bat|.lnk|.url|.appref-ms"
    Private lastModifiedDays As Integer
    Private directories As List(Of String)

    Private pm As PathManager

    Public Sub New(directories As List(Of String), lastModifiedDays As Integer)
        Me.lastModifiedDays = lastModifiedDays
        Me.directories = directories
        pm = New PathManager
    End Sub

    Private regainedSpace As Double
    Public ReadOnly Property GetRegainedSpace() As Double
        Get
            Return Math.Round(regainedSpace / 1000000, 2)
        End Get
    End Property

    Public Sub RemoveBackupedFiles()
        For Each myFile As String In Directory.GetFiles(pm.GetBackupPath)
            File.Delete(myFile)
        Next
    End Sub

    Public Sub CleanDirectories()
        For Each directoryPath As String In directories
            BackupFiles(directoryPath)
        Next
    End Sub

    Private Sub BackupFiles(directoryPath As String)
        For Each filePath As String In Directory.GetFiles(directoryPath)
            If Not IsForbiddenExt(Path.GetExtension(filePath)) Then
                If IsFileObsolete(File.GetLastWriteTime(filePath)) Then
                    MoveFile(filePath, Path.Combine(pm.GetBackupPath, Path.GetFileName(filePath))) 'pm.GetBackupPath & "\" & Path.GetFileName(filePath))
                End If
            End If
        Next
        CheckSubfolders(directoryPath)
    End Sub

    Private Function IsFileObsolete(lastWriteTime As Date) As Boolean
        Debug.Print(lastModifiedDays)
        Return DateAndTime.DateDiff(DateInterval.Day, lastWriteTime, DateTime.Now) > lastModifiedDays
    End Function

    Private Sub MoveFile(srcPath As String, destPath As String)
        Try
            File.Move(srcPath, destPath)
            Dim fi As FileInfo = New FileInfo(destPath)
            regainedSpace += fi.Length
        Catch
            'nothing
        End Try
    End Sub

    Private Function IsForbiddenExt(ext As String) As Boolean
        Return ForbiddenExtensionsString.IndexOf(ext) <> -1
    End Function

    Public Sub CheckSubfolders(directoryPath As String)
        For Each subfolderPath As String In Directory.GetDirectories(directoryPath)
            BackupFiles(subfolderPath)
            If IsDirectoryEmpty(subfolderPath) Then
                Directory.Delete(subfolderPath)
            End If
        Next
    End Sub

    Private Function IsDirectoryEmpty(directoryPath As String) As Boolean
        Return Directory.GetFiles(directoryPath).Count = 0 And Directory.GetDirectories(directoryPath).Count = 0
    End Function

End Class
